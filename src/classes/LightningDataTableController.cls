public class LightningDataTableController {
  
    @AuraEnabled
    public static List < String > updateAccounts(List<String> lstRecordId){
        List < String > oErrorMsg = new List < String > ();
        List < Account > lstAcc = new List < Account >();
        List < Account > lstRecToUpate = new List < Account >();
        if(lstRecordId.size() >0 && lstRecordId != null){
            // Query Records for delete where id in lstRecordId [which is pass from client side controller] 
            lstAcc = [select Id from Account where id IN: lstRecordId];
        }
        
        for(Account acc : lstAcc){
            acc.AccountSource = 'Partner Referral';
            lstRecToUpate.add(acc);
        }
        
        Database.SaveResult[] srList = Database.update(lstRecToUpate, false);
        // Database method to update the records in List
        
        // Iterate through each returned result by the method
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // This condition will be executed for successful records and will fetch
                // the ids of successful records
                System.debug('Successfully updated Account. Account ID is : ' + sr.getId());
            } else {
                // This condition will be executed for failed records
                oErrorMsg.add('');
                for (Database.Error err: sr.getErrors()) {
                    // add Error message to oErrorMsg list and return the list
                    oErrorMsg.add(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }
        return oErrorMsg;
    }
    
    @AuraEnabled
    public static List < String > removeRecords(List < String > lstRecordId, String idStr) {
        // for store Error Messages  
        List < String > oErrorMsg = new List < String > ();
        List < Account > lstDeleteRec = new List < Account >();
        if(idStr != null && idStr != ''){
           lstDeleteRec =  [SELECT Id FROM Account WHERE Id =: idStr];
        }
        if(lstRecordId.size() >0 && lstRecordId != null){
        // Query Records for delete where id in lstRecordId [which is pass from client side controller] 
        lstDeleteRec = [select Id from Account where id IN: lstRecordId];
         }
        
       Database.DeleteResult[] DR_Dels = Database.delete(lstDeleteRec, false);
        // Iterate through each returned result
        for (Database.DeleteResult dr: DR_Dels) {
            if (dr.isSuccess()) {
                system.debug('successful delete contact');
                // Operation was successful
            } else {
                // Operation failed, so get all errors   
                oErrorMsg.add('');
                for (Database.Error err: dr.getErrors()) {
                    // add Error message to oErrorMsg list and return the list
                    oErrorMsg.add(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }
        return oErrorMsg;
        
    }
    
            /*
        Method Name	: getAccRecords
        Purpose		: To get the wrapper of Columns and Headers
        */
            
    
    @AuraEnabled
    public static DataTableResponse getAccRecords(String strObjectName, String strFieldSetName){                
        
        //Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
        
        //To hold the table hearders 
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        
        //Field to be queried - fetched from fieldset
        List<String> lstFieldsToQuery = new List<String>();
        
        //The final wrapper response to return to component
        DataTableResponse response = new DataTableResponse();
        
        for( Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ){
            String dataType = String.valueOf(eachFieldSetMember.getType()).toLowerCase();
            //This way we can set the type of a column
            //We do not get the exact type from schema object which matches to lightning:datatable component structure
            if(dataType == 'datetime'){
                dataType = 'date';
            }
            //Create a wrapper instance and store label, fieldname and type.
            DataTableColumns datacolumns = new DataTableColumns( String.valueOf(eachFieldSetMember.getLabel()) , 
                                                                String.valueOf(eachFieldSetMember.getFieldPath()), 
                                                                String.valueOf(eachFieldSetMember.getType()).toLowerCase() );
            lstDataColumns.add(datacolumns);
            lstFieldsToQuery.add(String.valueOf(eachFieldSetMember.getFieldPath()));
        }
        
        //Form an SOQL to fetch the data - Set the wrapper instance and return as response
        if(! lstDataColumns.isEmpty()){            
            response.lstDataTableColumns = lstDataColumns;
            String query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM Account ORDER BY Name ASC  LIMIT 15';
            System.debug(query);
            response.lstDataTableData = Database.query(query);
        }
        
        return response;
    }
    
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }
    
    //Wrapper calss to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {
        @AuraEnabled
        public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled
        public List<sObject> lstDataTableData {get;set;}                
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    }
}