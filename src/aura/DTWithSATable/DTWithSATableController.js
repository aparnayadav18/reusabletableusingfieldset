({
    /*
    * Get records when page loads
    */
    doInit : function(component, event, helper) {
        helper.getDataHelper(component, event);
    },
    
    /*
    * Get records for selected page
    */
    onPageSelect : function(component, event, helper){
        var currentPage = parseInt(event.currentTarget.id);
        component.set("v.currentPageNumber",currentPage);
        helper.getSobjectsList(component);  
    },
    
    /*
    * Get prior page records from the current page
    */
    Previous : function(component,event, helper){
        var currentPage = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber",currentPage-1);
        helper.getSobjectsList(component);  
        
    },
    
    /*
    * Get next page records from the current page
    */
    Next : function(component, event, helper){
        var currentPage = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber",currentPage+1);
        helper.getSobjectsList(component);
    },
    
    /*
    * Get records for selected page size
    */
    selectedPageSize : function(component, event, helper){
        var pageSize = component.find("pageSelect").get("v.value");
        component.set("v.PageSize",pageSize);
        helper.getSobjectsList(component);
        console.log("pageSize::::",pageSize);
    },
    
    /*
    * Method to create new record for selected Object
    */
    createRecord : function(component, event, helper){
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Account"
        });
        createRecordEvent.fire();
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        
        switch (action.name) {
            case 'show_details':
                helper.showDetails(cmp, event,row);
                break;
            case 'edit':
                helper.editRecord(cmp,event,row)
                break;
            case 'delete':
                helper.removeRec(cmp,event,[],row.Id)
                break;
        }
    },
    handleRowSelection : function (cmp, event, helper) {
        var selectedRows  = event.getParam('selectedRows');
        var rows = [];
        for(var i=0;i<selectedRows.length;i++){
            rows.push(selectedRows[i].Id); 
        }
        console.log("rowss==== ",selectedRows);
        cmp.set("v.selectedRecords",rows);
    },
    deleteRecords : function (cmp, event, helper) {
        helper.removeRec(cmp,event,cmp.get("v.selectedRecords"),null);
    },
    massUpdateRecods : function (component, event, helper) {
        helper.updateRecs(component, event, helper);
    }
})