({
    getDataHelper : function(component, event) {
        var action = component.get("c.getAccRecords");
        //Set the Object parameters and Field Set name
        action.setParams({
            strObjectName : 'Account',
            strFieldSetName : 'AccountFieldSet'
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.FieldList", response.getReturnValue().lstDataTableColumns);
                this.setFieldAPIs(component);
                var data = response.getReturnValue().lstDataTableData;
                if(data.length >0){
                    for (var i = 0; i < data.length; i++) {
                        var row = data[i];
                        if (row.Owner){
                            row.OwnerName = row.Owner.Name;
                        }
                        if(row.CreatedBy){
                            row.CreatedByName = row.CreatedBy.Name; 
                        }
                        if(row.LastModifiedBy){
                            row.LastModifiedByName = row.LastModifiedBy.Name; 
                        }
                        if(row.Parent){
                            row.ParentName = row.Parent.Name;  
                        }
                    }
                    component.set("v.TotalRecords", data);
                    component.set("v.currentPageNumber",1);
                    //component.set("v.sortAsc", true);
                    this.getSobjectsList(component);
                }
                //component.set("v.data", data);    
            }else if (state === 'ERROR'){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }else{
                console.log('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action);	
    },
    setFieldAPIs : function(component){
        var actions = [
            { label: 'Show details', name: 'show_details' },
            { label: 'Edit', name: 'edit'},
            { label: 'Delete', name: 'delete' },
            
        ];
            
            var fieldAPI = component.get("v.FieldList");
            console.log("fieldAPI111----",fieldAPI.length);
            var columns = [];
        for (var i=0;i< fieldAPI.length; i++){
            var column = { label: '', fieldName: '',required : '', type: 'text'};
            
            if(fieldAPI[i].label == "Full Name" && fieldAPI[i].fieldName== "Owner.Name"){
                column.label = 'Account Owner';
                column.fieldName = 'OwnerName';
            }
            else if(fieldAPI[i].label == "Account Name" && fieldAPI[i].fieldName== "Parent.Name"){
                column.label = 'Parent Account';
                column.fieldName = 'ParentName';
            } else if(fieldAPI[i].label == "Account Type" && fieldAPI[i].fieldName== "Type"){
                column.label = 'Type';
                column.fieldName = fieldAPI[i].fieldName;
            } else if(fieldAPI[i].label == "Full Name" && fieldAPI[i].fieldName== "CreatedBy.Name"){
                column.label = 'Created By';
                column.fieldName = 'CreatedByName';
            } else if(fieldAPI[i].label == "Full Name" && fieldAPI[i].fieldName== "LastModifiedBy.Name"){
                column.label = 'Last Modified By';
                column.fieldName = 'LastModifiedByName';
            } else if(fieldAPI[i].label == "Account Description" && fieldAPI[i].fieldName== "Description"){
                column.label = 'Description';
                column.fieldName = fieldAPI[i].fieldName;
            } else if(fieldAPI[i].label == "Account Phone" && fieldAPI[i].fieldName== "Phone"){
                column.label = 'Phone';
                column.fieldName = fieldAPI[i].fieldName;
            } else if(fieldAPI[i].label == "Account Fax" && fieldAPI[i].fieldName== "Fax"){
                column.label = 'Fax';
                column.fieldName = fieldAPI[i].fieldName;
            } else if(fieldAPI[i].label == "Account Source" && fieldAPI[i].fieldName== "AccountSource"){
                column.label = fieldAPI[i].label;
                column.fieldName = fieldAPI[i].fieldName;
            }else{
                column.label = fieldAPI[i].label;
                column.fieldName = fieldAPI[i].fieldName;
            }
            column.type = fieldAPI[i].type;
            columns.push(column);
        }
        
        var fetchData = {
            AccountSource : {type : "picklist", values : [ 'Web', 'Phone Inquiry', 'Partner Referral', 'Purchased List','Other' ] },
         };
        
        var actionList = { type: 'action', typeAttributes: { rowActions: actions } };
        columns.push(actionList);
        console.log("columns;;;;;; ",columns);
        component.set('v.columns',columns);
    },
    getSobjectsList : function(component) {
        var lstRecord         = component.get("v.TotalRecords");
        var pageSize          = component.get("v.PageSize");
        var currentPageNumber = component.get("v.currentPageNumber");
        var start 			  = (currentPageNumber * pageSize) -pageSize;
        var end               = start + pageSize - 1;
        var lstLength 		  = Math.ceil(lstRecord.length/pageSize);
        if(lstLength > 0){
            var numberList = [];
            for(var i=1; i<=lstLength;i++){
                numberList.push(i);
            }
            component.set("v.TotalPages",numberList);
        }
        var lstRecords  = [];
        for(var i = start;i<=end;i++ ){
            if(lstRecord.length>i){
                lstRecords.push(lstRecord[i]);
            }
        }
        component.set("v.data",lstRecords);
    },
    showDetails : function(cmp, event,row){
        cmp.set("v.recordId",row.Id);
        cmp.set("v.isRecord",true);
    },
    
    editRecord: function (cmp,event, row) {
        console.log('Row 1111--',row); 
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": row.Id
        });
        editRecordEvent.fire();
        
    },
    
    
    removeRec : function(component, event,lstRecordIds, selectedRecordId) {
        console.log("selectedRows-----",lstRecordIds);
        if((lstRecordIds != null && lstRecordIds.length > 0) || (selectedRecordId != null && selectedRecordId != '')){
            //call apex class method
            var action = component.get('c.removeRecords');
            // pass the all selected record's Id's to apex method 
            action.setParams({
                "lstRecordId":lstRecordIds,
                "idStr" : selectedRecordId
            });
            action.setCallback(this, function(response) {
                //store state of response
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log(state);
                    if (response.getReturnValue() != '' && response.getReturnValue() != undefined) {
                        // if getting any error while delete the records , then display a alert msg/
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "The following error has occurred. while deleting record-->"+response.getReturnValue(),
                            "key":"info",
                            "type":"error",
                        });
                        toastEvent.fire();
                    } else {
                        console.log("Record Deleted");
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "message": "The record(s) has been deleted."
                        });
                        resultsToast.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                }
            });
            $A.enqueueAction(action); 
        }
        else{
            alert('Please select a record to delete');
        }
        
    },
    updateRecs : function(component, event, helper){
        var selRecs = component.get("v.selectedRecords");
        if(selRecs.length>0){
            var action = component.get('c.updateAccounts');
            // pass the all selected record's Id's to apex method 
            action.setParams({
                "lstRecordId":selRecs,
            });
            action.setCallback(this, function(response) {
                //store state of response
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log(state);
                    if (response.getReturnValue() != '' && response.getReturnValue() != undefined) {
                        // if getting any error while delete the records , then display a alert msg/
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "The following error has occurred. while updating record-->"+response.getReturnValue(),
                            "key":"info",
                            "type":"error",
                        });
                        toastEvent.fire();
                    } else {
                        console.log("Record Deleted");
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "message": "The record(s) has been updated."
                        });
                        resultsToast.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                }
            });
            $A.enqueueAction(action); 
        }
        else{
            alert('Please select the record');
        }
    }
})