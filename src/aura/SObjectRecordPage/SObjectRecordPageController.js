({
	editRecord : function(component, event, helper) {
		var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.recordId"),
        });
        editRecordEvent.fire();
	},
    
    closeRecord : function(component, event, helper) {
        component.set("v.isRecord",false);
	}
})